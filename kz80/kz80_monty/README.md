# Z80 Monty

## Monty Repo

* Monty by John Hardy
* https://github.com/jhlagado/monty


## Information

This particular version of Monty expects a MO6850 ACIA serial chip.  Like the other ROMs, this serial chip is emulated by the Arduino Mega.
