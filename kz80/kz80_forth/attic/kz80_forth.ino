////////////////////////////////////////////////////////////////////
// Z80 RetroShield Code w/ EFEX Monitor
// 2019/08/12
// Version 1.0
// Erturk Kocalar
//
// The MIT License (MIT)
//
// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 8/12/2019    Initial Release (BASIC).                            E. Kocalar
//
////////////////////////////////////////////////////////////////////
// Options
//   USE_SPI_RAM: Enable Microchip 128KB SPI-RAM  (Details coming up)
//   USE_LCD_KEYPAD: Enable LCD/Keyboard Shield
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define USE_SPI_RAM     0
#define USE_LCD_KEYPAD  1
#define outputDEBUG     1

////////////////////////////////////////////////////////////////////
// include the library code for LCD shield:
////////////////////////////////////////////////////////////////////
#include <avr/pgmspace.h>


////////////////////////////////////////////////////////////////////
// SPI FUNCTIONS
//
// 23L1024 SPI RAM (128K)
// $00000 - $1FFFF
////////////////////////////////////////////////////////////////////
#if (USE_SPI_RAM)

#include "pins2_arduino.h"
#include <DIO2.h>

GPIO_pin_t LED1          = DPA4;
GPIO_pin_t LED2          = DPA5;

GPIO_pin_t PIN_SIO3      = DPA3;
GPIO_pin_t PIN_SIO2      = DPA2;
GPIO_pin_t PIN_SIO1      = DPA1;
GPIO_pin_t PIN_SIO0      = DPA0;
GPIO_pin_t PIN_HOLD      = DP7;
GPIO_pin_t PIN_SCK       = DP6;
GPIO_pin_t PIN_SI        = DP5;
GPIO_pin_t PIN_SO        = DP3;
GPIO_pin_t PIN_CS        = DP2;

inline __attribute__((always_inline))
void spi_reset()
{
  digitalWrite2f(PIN_CS, LOW);
  spi_send(0xFF);
  digitalWrite2f(PIN_CS, HIGH);
}

inline __attribute__((always_inline))
byte spi_get_mode()
{
  byte mode;

  digitalWrite2f(PIN_CS, LOW);
  spi_send(0x05);
  mode = spi_receive();
  digitalWrite2f(PIN_CS, HIGH);
  return mode;
}

inline __attribute__((always_inline))
byte spi_set_mode(byte mode)
{
  digitalWrite2f(PIN_CS, LOW);
  spi_send(0x01);
  spi_send(mode);
  digitalWrite2f(PIN_CS, HIGH);
  return mode;
}

inline __attribute__((always_inline))
byte spi_get_mode_quad()
{
  byte mode;

  digitalWrite2f(PIN_CS, LOW);
  spi_send_quad(0x05);
  mode = spi_receive_quad();
  digitalWrite2f(PIN_CS, HIGH);
  return mode;
}

inline __attribute__((always_inline))
void spi_enter_quad()
{
  digitalWrite2f(PIN_CS, LOW);
  spi_send(0x38);
  digitalWrite2f(PIN_CS, HIGH);
  pinMode2f(PIN_SI, INPUT_PULLUP);
}

inline __attribute__((always_inline))
void spi_exit_quad()
{
  digitalWrite2f(PIN_CS, LOW);
  spi_send(0xFF);
  digitalWrite2f(PIN_CS, HIGH);
  pinMode2f(PIN_SI, OUTPUT);  digitalWrite2f(PIN_SI, LOW);
}

inline __attribute__((always_inline))
void spi_init()
{
  pinMode2f(PIN_CS,   OUTPUT);  digitalWrite2f(PIN_CS, HIGH);
  pinMode2f(PIN_SCK,  OUTPUT);  digitalWrite2f(PIN_SCK,  LOW);
  pinMode2f(PIN_HOLD, INPUT_PULLUP);
  pinMode2f(PIN_SI,   OUTPUT);  digitalWrite2f(PIN_SI, LOW);
  pinMode2f(PIN_SO,   INPUT_PULLUP);
  pinMode2f(PIN_SIO0, INPUT_PULLUP);
  pinMode2f(PIN_SIO1, INPUT_PULLUP);
  pinMode2f(PIN_SIO2, INPUT_PULLUP);
  pinMode2f(PIN_SIO3, INPUT_PULLUP);

  pinMode2f(LED1, OUTPUT);      digitalWrite2f(LED1, LOW);
  pinMode2f(LED2, OUTPUT);      digitalWrite2f(LED2, HIGH);

  spi_reset();
  spi_set_mode(0x40);   // sequantial mode

  spi_enter_quad();
  Serial.print("\nSPI-RAM -   Quad Mode: ");
  Serial.println(spi_get_mode_quad(), HEX);
}

inline __attribute__((always_inline))
void spi_send(byte working)         // function to actually bit shift the data byte out
{
  pinMode2f(PIN_SI,   OUTPUT);
  digitalWrite2f(PIN_SI,   LOW);

  for(int i = 1; i <= 8; i++)         // setup a loop of 8 iterations, one for each bit
  {
      if (working > 127)              // test the most significant bit
      {
          digitalWrite2f(PIN_SI,HIGH);   // if it is a 1 (ie. B1XXXXXXX), set the master out pin high
          // digitalWrite2f(LED2, HIGH);
      } else {
          digitalWrite2f(PIN_SI, LOW);   // if it is not 1 (ie. B0XXXXXXX), set the master out pin low
          // digitalWrite2f(LED2, LOW);
      }
      digitalWrite2f(PIN_SCK,HIGH);        // set clock high, the pot IC will read the bit into its register
      // digitalWrite2f(LED1, HIGH);
      working = working << 1;
      // delay(500);
      digitalWrite2f(PIN_SCK,LOW);          // set clock low, the pot IC will stop reading and prepare for the next iteration (next significant bit
      // digitalWrite2f(LED1, LOW);
      // delay(500);
  }
  pinMode2f(PIN_SI,   INPUT_PULLUP);
}

inline __attribute__((always_inline))
byte spi_receive()
{
  byte din = 0;

  for(int i = 1; i <= 8; i++)         // setup a loop of 8 iterations, one for each bit
  {
    din = din << 1;
    digitalWrite2f (PIN_SCK,HIGH);        // set clock high, the pot IC will read the bit into its register
    if (digitalRead2f(PIN_SO))
      din = din | 1;
    digitalWrite2f(PIN_SCK,LOW);          // set clock low, the pot IC will stop reading and prepare for the next iteration (next significant bit
  }
  return(din);
}

inline __attribute__((always_inline))
void spi_write_byte(byte bank, word addr, byte data)
{
  byte hh = (addr & 0xFF00) >> 8;
  byte ll = addr & 0xFF;

  digitalWrite2f(PIN_CS, LOW);
  spi_send(0x02);
  spi_send(bank & 0x01);
  spi_send(hh);
  spi_send(ll);
  spi_send(data);
  digitalWrite2f(PIN_CS, HIGH);
}

inline __attribute__((always_inline))
byte spi_read_byte(byte bank, word addr)
{
  byte hh = (addr & 0xFF00) >> 8;
  byte ll = addr & 0xFF;
  byte data;

  digitalWrite2f(PIN_CS, LOW);
  spi_send(0x03);
  spi_send(bank & 0x01);
  spi_send(hh);
  spi_send(ll);
  data = spi_receive();
  digitalWrite2f(PIN_CS, HIGH);
  return data;
}

inline __attribute__((always_inline))
byte spi_read_byte_quad(byte bank, word addr)
{
  byte hh = (addr & 0xFF00) >> 8;
  byte ll = addr & 0xFF;
  byte data;

  digitalWrite2f(PIN_CS, LOW);
  spi_send_quad(0x03);
  spi_send_quad(bank & 0x01);
  spi_send_quad(hh);
  spi_send_quad(ll);
  /* dummy read */ spi_receive_quad();
  data = spi_receive_quad();
  digitalWrite2f(PIN_CS, HIGH);

  // Serial.print("SPI_Read: "); Serial.print(data, HEX); Serial.print(" <- "); Serial.println(addr, HEX);
  return data;
}

inline __attribute__((always_inline))
void spi_write_byte_quad(byte bank, word addr, byte data)
{
  byte hh = (addr & 0xFF00) >> 8;
  byte ll = addr & 0xFF;

  // Serial.print("SPI_Write: "); Serial.print(data, HEX); Serial.print(" -> "); Serial.println(addr, HEX);

  digitalWrite2f(PIN_CS, LOW);
  spi_send_quad(0x02);
  spi_send_quad(bank & 0x01);
  spi_send_quad(hh);
  spi_send_quad(ll);
  spi_send_quad(data);
  digitalWrite2f(PIN_CS, HIGH);
}

inline __attribute__((always_inline))
void spi_read_byte_array_quad(byte bank, word addr, word cnt, byte *ptr)
{
  byte hh = (addr & 0xFF00) >> 8;
  byte ll = addr & 0xFF;
  byte data;

  digitalWrite2f(PIN_CS, LOW);
  spi_send_quad(0x03);
  spi_send_quad(bank & 0x01);
  spi_send_quad(hh);
  spi_send_quad(ll);
  /* dummy read */ spi_receive_quad();
  while (cnt--)
    *ptr++ = spi_receive_quad();
  digitalWrite2f(PIN_CS, HIGH);
}

inline __attribute__((always_inline))
void spi_write_byte_array_quad(byte bank, word addr, word cnt, byte *ptr)
{
  byte hh = (addr & 0xFF00) >> 8;
  byte ll = addr & 0xFF;

  digitalWrite2f(PIN_CS, LOW);
  spi_send_quad(0x02);
  spi_send_quad(bank & 0x01);
  spi_send_quad(hh);
  spi_send_quad(ll);
  while (cnt--)
  {
    Serial.print("+"); Serial.print(*ptr, HEX);
    spi_send_quad(*ptr++);
  }
  Serial.println("");
  digitalWrite2f(PIN_CS, HIGH);
}

inline __attribute__((always_inline))
void spi_send_quad(byte working)         // Quad mode
{
  byte hh = (working & 0xF0) >> 4;
  byte ll = (working & 0x0F);

  // digitalWrite2f(LED1, HIGH);

  pinMode2f(PIN_SIO0, OUTPUT);
  pinMode2f(PIN_SIO1, OUTPUT);
  pinMode2f(PIN_SIO2, OUTPUT);
  pinMode2f(PIN_SIO3, OUTPUT);

  PORTF = (PINF & 0xF0) | hh;

  digitalWrite2f(PIN_SCK,HIGH);
  digitalWrite2f(PIN_SCK,LOW);

  PORTF = (PINF & 0xF0) | ll;

  digitalWrite2f(PIN_SCK,HIGH);
  digitalWrite2f(PIN_SCK,LOW);

  pinMode2f(PIN_SIO0, INPUT_PULLUP);
  pinMode2f(PIN_SIO1, INPUT_PULLUP);
  pinMode2f(PIN_SIO2, INPUT_PULLUP);
  pinMode2f(PIN_SIO3, INPUT_PULLUP);

  // digitalWrite2f(LED1, LOW);
}

inline __attribute__((always_inline))
word spi_receive_quad()         // Quad mode
{
  byte b = 0;

  // digitalWrite2f(LED2, HIGH);

  digitalWrite2f(PIN_SCK, HIGH);
  b = (PINF & 0x0F) << 4;
  digitalWrite2f(PIN_SCK,LOW);
  digitalWrite2f(PIN_SCK,HIGH);
  b = b | (PINF & 0x0F);
  digitalWrite2f(PIN_SCK,LOW);

  // digitalWrite2f(LED2, LOW);

  return(b);
}


////////////////////////////////////////////////////////////////////
// Cache for SPI-RAM
////////////////////////////////////////////////////////////////////

byte cachePage[16];
byte cacheRAM[16][256];

inline __attribute__((always_inline))
byte cache_read_byte(word addr)           // 0x1234
{
//  byte p = (addr & 0xFF00) >> 8;          // p = 0x12
//  byte a = addr & 0x00FF;                 // a = 0x34
//  byte n = a >> 4;                        // n = 0x03
//  byte r = a & 0x0F;                      // r = 0x04

  byte a = (addr & 0xFF00) >> 8;            // a = 0x12
  byte p = a >> 4;                          // p = 0x01
  byte n = a & 0x0F;                        // n = 0x02
  byte r = (addr & 0x00FF);                 // r = 0x34

  // Serial.print("cache addr: "); Serial.print(addr, HEX);

  if (cachePage[n] == p)
  {
    // Cache Hit !!!
    return cacheRAM[n][r];
  }
  else
  {
    // Need to fill cache from SPI-RAM
    digitalWrite2f(LED2, HIGH);
    spi_read_byte_array_quad(0, addr & 0xFF00, 256, cacheRAM[n]);
    cachePage[n] = p;
    digitalWrite2f(LED2, LOW);
    return cacheRAM[n][r];
  }
}

inline __attribute__((always_inline))
void cache_write_byte(word addr, byte din)   // 0x1234
{
//  byte p = (addr & 0xFF00) >> 8;          // p = 0x12
//  byte a = addr & 0x00FF;                 // a = 0x34
//  byte n = a >> 4;                        // n = 0x03
//  byte r = a & 0x0f;                      // r = 0x04

  byte a = (addr & 0xFF00) >> 8;            // a = 0x12
  byte p = a >> 4;                          // p = 0x01
  byte n = a & 0x0F;                        // n = 0x02
  byte r = (addr & 0x00FF);                 // r = 0x34


  if (cachePage[n] == p)
  {
    // Cache Hit !!!
    cacheRAM[n][r] = din;
    spi_write_byte_quad(0, addr, din);        // Write-thru cache :)
    return;
  }
  else
  {
    // Need to fill cache from SPI-RAM
    digitalWrite2f(LED1, HIGH);
    spi_write_byte_quad(0, addr, din);
    spi_read_byte_array_quad(0, addr & 0xFF00, 256, cacheRAM[n]);
    cachePage[n] = p;
    digitalWrite2f(LED1, LOW);
    return;
  }
}

void cache_init()
{
  // Initialize cache from spi-ram
  for(int p=0; p<16; p++)
  {
    cachePage[p] = 0;
  }
  Serial.println("RAM Cache - Initialized.");

}

#endif


////////////////////////////////////////////////////////////////////
// Configuration
////////////////////////////////////////////////////////////////////
#if USE_LCD_KEYPAD

#include <LiquidCrystal.h>

  /*
    The circuit:
   * LCD RS pin to digital pin 12
   * LCD Enable pin to digital pin 11
   * LCD D4 pin to digital pin 5
   * LCD D5 pin to digital pin 4
   * LCD D6 pin to digital pin 3
   * LCD D7 pin to digital pin 2
   * LCD R/W pin to ground
   * 10K resistor:
   * ends to +5V and ground
   * wiper to LCD VO pin (pin 3)
  */

  #define LCD_RS  8
  #define LCD_EN  9
  #define LCD_D4  4
  #define LCD_D5  5
  #define LCD_D6  6
  #define LCD_D7  7
  #define LCD_BL  10
  #define LCD_BTN  0

  #define NUM_KEYS   5
  #define BTN_DEBOUNCE 10
  #define BTN_RIGHT  0
  #define BTN_UP     1
  #define BTN_DOWN   2
  #define BTN_LEFT   3
  #define BTN_SELECT 4
  const int adc_key_val[NUM_KEYS] = { 30, 180, 360, 535, 760 };
  int       key = -1;
  int       oldkey = -1;
  boolean   BTN_PRESS = 0;
  boolean   BTN_RELEASE = 0;

  LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);
  int backlightSet = 25;
#endif

////////////////////////////////////////////////////////////////////
// Z80 DEFINITIONS
////////////////////////////////////////////////////////////////////

// Z80 HW CONSTRAINTS
// !!! TODO !!!
// 1- RESET_N must be asserted at least 2 clock cycles.
// 2- CLK can not be low more than 5 microseconds.  Can be high indefinitely.
//

////////////////////////////////////////////////////////////////////
// MEMORY LAYOUT
////////////////////////////////////////////////////////////////////

#if (USE_SPI_RAM)
  // 2K MEMORY
  #define RAM_START   0xF800
  #define RAM_END     0xFFFF
  byte    RAM[RAM_END-RAM_START+1];
#else
  // 6K MEMORY
  #define RAM_START   0x2000
  //E800
  #define RAM_END     0x37FF
  //FFFF
  byte    RAM[RAM_END-RAM_START+1];
#endif

////////////////////////////////////////////////////////////////////
// Forth Code
////////////////////////////////////////////////////////////////////
// static const unsigned char

#include "rom_bin.h"

////////////////////////////////////////////////////////////////////

// ROM(s)
#define ROM_START   0x0000
#define ROM_END     (ROM_START+sizeof(rom_bin)-1)


////////////////////////////////////////////////////////////////////
// 6850 Peripheral
////////////////////////////////////////////////////////////////////

/*

STATUS Register bitmasks

7   6   5   4   3   2   1   0
-------------------------------
IRQ TDRE TC  RDRF FE  OVRN PE  IRQ

- Bit 7: IRQ (also mirrored at Bit 0) - Interrupt Request (set when an interrupt condition exists, reset when the interrupt is acknowledged)
- Bit 6: TDRE - Transmitter Data Register Empty (set when the transmit data register is empty)
- Bit 5: TC - Transmit Control (set when the last character in the transmit data register has been sent)
- Bit 4: RDRF - Receiver Data Register Full (set when a character has been received and is ready to be read from the receive data register)
- Bit 3: FE - Frame Error (set when the received character does not have a valid stop bit)
- Bit 2: OVRN - Overrun (set if a character is received before the previous one is read)
- Bit 1: PE - Parity Error (set when the received character has incorrect parity)
- Bit 0: Mirrors the IRQ bit

*/

/*

STATUS REGISTER

7	6	5		4	3	2		1		0
IRQ	PE	OVRN	FE!	CTS	!DCD	TDRE	RDRF

IRQ –Interrupt request
set whenever the ACIA wishes to interrupt CPU:
	–Received data register full (SR bit 0 set)
	–Transmitter data register empty (SR bit 1 set)
!DCD bit set (SR bit 2)

PE –Parity error
set when the parity bit received does not match the parity bit generated locally for the received data

OVRN –Receiver Overrun•set when data is received by the ACIA and not read by the CPU when new data is received over
	-writing old data
	it indicates that data has been lost

FE –Framing error
set when received data is incorrectly framed by the start and stop bits

!CTS –Clear to send•directly indicates the status of the ACIA’s

!CTS input

!DCD –Data Carrier Detect•set when the ACIA’s!DCD input is high
	reset when the CPU reads both the status register and the data register or when ACIA is master reset

TDRE -Transmitter data register empty
set when the transmitter data register is empty, indicating that data has been sent
	reset when transmitter data register is full or when !CTS is high, indicating that the peripheral is not ready

	RDRF –Receiver data register full•set when the receiver data register is full, indicating that data has been received
	reset when the data has been read from the data register


 */


#define ADDR_6850_DATA        0x81
#define ADDR_6850_CONTROL     0x80

byte reg6850_DATA;
byte reg6850_CONTROL;
byte reg8251_STATE;      // register to keep track of 8251 state: reset or initialized
byte reg8251_MODE;
byte reg8251_COMMAND;
byte reg8251_STATUS;
byte reg8251_DATA;

////////////////////////////////////////////////////////////////////
// Z80 Processor Control
////////////////////////////////////////////////////////////////////
//

/* Digital Pin Assignments */
#define DATA_OUT (PORTL)
#define DATA_IN  (PINL)
#define ADDR_H   (PINC)
#define ADDR_L   (PINA)
#define ADDR     ((unsigned int) (ADDR_H << 8 | ADDR_L))

#define uP_RESET_N  38
#define uP_MREQ_N   41
#define uP_IORQ_N   39
#define uP_RD_N     53
#define uP_WR_N     40
#define uP_NMI_N    51
#define uP_INT_N    50
#define uP_CLK      52

// Fast routines to drive clock signals high/low; faster than digitalWrite
// required to meet >100kHz clock
//
#define CLK_HIGH      (PORTB = PORTB | 0x02)
#define CLK_LOW       (PORTB = PORTB & 0xFC)
#define STATE_RD_N    (PINB & 0x01)
#define STATE_WR_N    (PING & 0x02)
#define STATE_MREQ_N  (PING & 0x01)
#define STATE_IORQ_N  (PING & 0x04)

#define DIR_IN  0x00
#define DIR_OUT 0xFF
#define DATA_DIR   DDRL
#define ADDR_H_DIR DDRC
#define ADDR_L_DIR DDRA

unsigned long clock_cycle_count;
unsigned long clock_cycle_last;
unsigned long uP_start_millis;
unsigned long uP_stop_millis;
unsigned long uP_millis_last;
unsigned int  uP_ADDR;
byte uP_DATA;

void uP_init()
{
  // Set directions
  DATA_DIR = DIR_IN;
  ADDR_H_DIR = DIR_IN;
  ADDR_L_DIR = DIR_IN;

  pinMode(uP_RESET_N, OUTPUT);
  pinMode(uP_WR_N, INPUT);
  pinMode(uP_RD_N, INPUT);
  pinMode(uP_MREQ_N, INPUT);
  pinMode(uP_IORQ_N, INPUT);
  pinMode(uP_INT_N, OUTPUT);
  pinMode(uP_NMI_N, OUTPUT);
  pinMode(uP_CLK, OUTPUT);

  uP_assert_reset();
  digitalWrite(uP_CLK, LOW);

  clock_cycle_count = 0;
  clock_cycle_last  = 0;
  uP_start_millis = millis();
  uP_millis_last = millis();
}

void uP_assert_reset()
{
  // Drive RESET conditions
  digitalWrite(uP_RESET_N, LOW);
  digitalWrite(uP_INT_N, HIGH);
  digitalWrite(uP_NMI_N, HIGH);
}

void uP_release_reset()
{
  // Drive RESET conditions
  digitalWrite(uP_RESET_N, HIGH);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

// Motorola MC6850 Asynchronous Communications Interface Adapter (ACIA) emulation

// http://www.cpcwiki.eu/index.php/6850_ACIA_chip
// http://www.cpcwiki.eu/imgs/3/3f/MC6850.pdf
//


        // +--------+----------------------------------------------------------------------------------+
        // |        |                                Buffer address                                    |
        // |        +------------------+------------------+--------------------+-----------------------+
        // |        |            _     |            _     |              _     |               _       |
        // |  Data  |     RS * R/W     |     RS * R/W     |       RS * R/W     |        RS * R/W       |
        // |  Bus   |   (high)(low)    |   (high)(high)   |      (low)(low)    |       (low)(low)      |
        // |  Line  |     Transmit     |      Receive     |                    |                       |
        // | Number |       Data       |        Data      |        Control     |         Status        |
        // |        |     Register     |      Register    |       Register     |        register       |
        // |        +------------------+------------------+--------------------+-----------------------+
        // |        |  (Write only)    |   (Read only)    |       (Write only) |      (Read only)      |
        // +--------+------------------+------------------+--------------------+-----------------------+
        // |    0   |    Data bit 0*   |    Data bit 0    |   Counter divide   | Receive data register |
        // |        |                  |                  |   select 1 (CR0)   |      full (RDRF)      |
        // +--------+------------------+------------------+--------------------+-----------------------+
        // |    1   |    Data bit 1    |    Data bit 1    |   Counter divide   | Transmit data register|
        // |        |                  |                  |   select 2 (CR1)   |     empty (TDRE)      |
        // +--------+------------------+------------------+--------------------+-----------------------+
        // |    2   |    Data bit 2    |    Data bit 2    |   Word select 1    |  Data carrier detect  |
        // |        |                  |                  |          (CR2)     |      (DCD active)     |
        // +--------+------------------+------------------+--------------------+-----------------------+
        // |    3   |    Data bit 3    |    Data bit 3    |   Word select 1    |     Clear to send     |
        // |        |                  |                  |          (CR3)     |      (CTS active)     |
        // +--------+------------------+------------------+--------------------+-----------------------+
        // |    4   |    Data bit 4    |    Data bit 4    |   Word select 1    |     Framing error     |
        // |        |                  |                  |          (CR4)     |          (FE)         |
        // +--------+------------------+------------------+--------------------+-----------------------+
        // |    5   |    Data bit 5    |    Data bit 5    | Transmit control 1 |    Receiver overrun   |
        // |        |                  |                  |          (CR5)     |        (OVRN)         |
        // +--------+------------------+------------------+--------------------+-----------------------+
        // |    6   |    Data bit 6    |    Data bit 6    | Transmit control 2 |    Parity error (PE)  |
        // |        |                  |                  |          (CR6)     |                       |
        // +--------+------------------+------------------+--------------------+-----------------------+
        // |    7   |    Data bit 7*** |    Data bit 7**  | Receive interrupt  |    Interrupt request  |
        // |        |                  |                  |   enable (CR7)     |      (IRQ active)     |
        // +--------+------------------+------------------+--------------------+-----------------------+
		    //		  * Leading bit = LSB = Bit 0
		    //	   ** Data bit will be zero in 7-bit plus parity modes
		    //	  *** Data bit is "don't care" in 7-bit plus parity modes



////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////
// This is where the action is.
// it reads processor control signals and acts accordingly.
//
// Z80 takes multiple cycles to accomplish each rd/write.
// so if we act on IORQ, RD, WR at every clock cycle, then
// we perform erroneous extra IO read/writes.
// FIX WR: perform IO write only on IORQ/WR falling edge.
// FIX RD: perform IO read only on IORQ/RD falling edge.
byte prevIORQ = 0;
byte prevMREQ = 0;
byte prevDATA = 0;

inline __attribute__((always_inline))
void cpu_tick()
{
  if ((clock_cycle_count % 5000) == 0)
  {
    char tmp[20];
    float freq;

    lcd.setCursor(0, 0);
    // lcd.print(clock_cycle_count);
    sprintf(tmp, "A=%0.4X D=%0.2X", uP_ADDR, DATA_OUT);
    lcd.print(tmp);
    lcd.setCursor(0,1);

    freq = (float) (clock_cycle_count - clock_cycle_last) / (millis() - uP_millis_last + 1);
    lcd.print(freq);  lcd.print(" kHz   Z80");
    clock_cycle_last = clock_cycle_count;
    uP_millis_last = millis();
  }

  /*
    SerialEvent occurs whenever a new data comes in the
  hardware serial RX.  This routine is run between each
  time loop() runs, so using delay inside loop can delay
  response. Note: Multiple bytes of data may be available.
  */
  if (Serial.available())
  {
    digitalWrite(uP_INT_N, LOW);
  }

  CLK_HIGH;    // CLK goes high

  uP_ADDR = ADDR;

  // unlike memory mapped devices in 6502 & 6809,
  // Z80 bus has two modes: Memory (MREQ_N) and IO (IORQ_N)

  //////////////////////////////////////////////////////////////////////
  // Memory Access?
  if (!STATE_MREQ_N)
  {

    // Memory Read?
    if (!STATE_RD_N)
    {
      // change DATA port to output to uP:
      DATA_DIR = DIR_OUT;

      // ROM?
      if ( (ROM_START <= uP_ADDR) && (uP_ADDR <= ROM_END) )
        DATA_OUT = pgm_read_byte_near(rom_bin + (uP_ADDR - ROM_START));
      else
      // RAM?
      if ( (uP_ADDR <= RAM_END) && (RAM_START <= uP_ADDR) )
        DATA_OUT = RAM[uP_ADDR - RAM_START];

#if (USE_SPI_RAM)
      else
      {
        //treat everywhere else as ram
        DATA_OUT = cache_read_byte(uP_ADDR);
        // DATA_OUT = spi_read_byte_quad(0, uP_ADDR);
      }
#endif

/*
#if outputDEBUG
      char tmp[20];
      sprintf(tmp, "-- A=%0.4X D=%0.2X\n", uP_ADDR, DATA_OUT);
      Serial.write(tmp);
#endif
*/
    } else
    // Write?
    if (!STATE_WR_N)
    {
      // Memory Write
      if ( (RAM_START <= uP_ADDR) && (uP_ADDR <= RAM_END) )
        RAM[uP_ADDR - RAM_START] = DATA_IN;
#if (USE_SPI_RAM)
      else
      {
        // treat everywhere else as ram
        cache_write_byte(uP_ADDR, DATA_IN);
        // spi_write_byte_quad(0, uP_ADDR, DATA_IN);
      }
#endif

#if outputDEBUG
/*
      char tmp[20];
      sprintf(tmp, "WR A=%0.4X D=%0.2X\n", uP_ADDR, DATA_IN);
      Serial.write(tmp);
*/
#endif
    }

  } else

  //////////////////////////////////////////////////////////////////////
  // IO Access?

  if (!STATE_IORQ_N)
  {
    // IO Read?
    if (!STATE_RD_N && prevIORQ)
    {
      DATA_DIR = DIR_OUT;

      // 6850 access
      if (ADDR_L == ADDR_6850_DATA)
      {
        prevDATA = reg6850_DATA = Serial.read(); 
        // You might also want to handle any control bits for 6850 here

        Serial.write(reg6850_DATA);

        digitalWrite(uP_INT_N, HIGH);
      }
      else if (ADDR_L == ADDR_6850_CONTROL) // Change the address as per your design
      {
      
        if(Serial.available()) {
          
          prevDATA = reg6850_DATA = Serial.read();
          Serial.write(reg6850_DATA);
        } else {
          prevDATA = reg6850_CONTROL;
        }
      }

      DATA_OUT = prevDATA;
    }
    else if (!STATE_RD_N && !prevIORQ)
    {
      DATA_DIR = DIR_OUT;
      DATA_OUT = prevDATA;
    }
    else if (!STATE_WR_N && prevIORQ)
    {
      /*
      ************** Read from Z80, write to Serial ************** 
      */
      // 6850 access
      if (ADDR_L == ADDR_6850_DATA)
      {
        reg6850_DATA = DATA_IN;
        Serial.write(reg6850_DATA);
      }
      else if (ADDR_L == ADDR_6850_CONTROL)
      {
        // reg6850_CONTROL gets set here and then used in the READ phase when ADDR_L is ADDR_6850_CONTROL
        reg6850_CONTROL = DATA_IN;
      }
    }
    else
    {
      DATA_DIR = DIR_OUT;
      DATA_OUT = 0;
    }

#if (outputDEBUG)
    {
      char tmp[20];
      sprintf(tmp, "\nIORQ RW=%0.1X A=%0.4X D=%0.2X\n", STATE_WR_N, uP_ADDR, DATA_OUT);
      // Serial.write(tmp);
    }
#endif
  }

  prevIORQ = STATE_IORQ_N;
  prevMREQ = STATE_MREQ_N;

  //////////////////////////////////////////////////////////////////////
  // start next cycle
  CLK_LOW;    // E goes low

  // one full cycle complete
  clock_cycle_count ++;

  // natural delay for DATA Hold time (t_HR)
  DATA_DIR = DIR_IN;

}


////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////

void setup()
{

  Serial.begin(115200);

  Serial.println("Configuration:");
  Serial.println("==============");
  Serial.print("Debug:      "); Serial.println(outputDEBUG, HEX);
  Serial.print("LCD-DISP:   "); Serial.println(USE_LCD_KEYPAD, HEX);
  Serial.print("SPI-RAM:    "); Serial.print(USE_SPI_RAM * 65536, DEC); Serial.println(" Bytes");
  Serial.print("SRAM Size:  "); Serial.print(RAM_END - RAM_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM_START: 0x"); Serial.println(RAM_START, HEX);
  Serial.print("SRAM_END:   0x"); Serial.println(RAM_END, HEX);
  Serial.println("");


#if (USE_LCD_KEYPAD)
  pinMode(LCD_BL, OUTPUT);
  analogWrite(LCD_BL, 100);
  lcd.begin(16, 2);

#endif

#if (USE_SPI_RAM)
  // Initialize memory subsystem
  spi_init();
  cache_init();
#endif

  // Initialize processor GPIO's
  uP_init();

  // Reset processor
  //
  uP_assert_reset();
  for(int i=0;i<25;i++) cpu_tick();

  // Go, go, go
  uP_release_reset();

  Serial.println("\n");
}

////////////////////////////////////////////////////////////////////
// Loop()
// * This function runs in parallel with timer interrupt handler.
//   i.e. simplest multi-threading.
// * try to be done as quickly as possible so processor does not
//   slow down.
////////////////////////////////////////////////////////////////////

void loop()
{

  // Loop forever
  //
  cpu_tick();

}
