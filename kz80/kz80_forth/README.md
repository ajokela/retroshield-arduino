# Z80 Forth

## Forth Repo

* Forth by John Hardy
* https://github.com/jhlagado/firth 
* https://github.com/jhlagado/stc-forth

## Information

This particular version of Forth expects a MO6850 ACIA serial chip.  Like the other ROMs, this serial chip is emulated by the Arduino Mega.
