# RetroShield 6502 for Arduino Mega

These are the software project folders for the RetroShield 6502.

* `k65c02_apple1`: emulates Apple I hardware, includes BIOS and BASIC ROMs.
* `k65c02_kim1`: emulates KIM-1 hardware thru TTY. (limited testing.)
* `k65c02_c64`: emulates C64 hardware to run BASIC thru VT100.